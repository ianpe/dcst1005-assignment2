
# Enter PSsession on target server
Enter-PSSession magnaaro-srv

#############################
######## IIS webpage ########
#############################

# Prerequisites
# - Choco is installed
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
# - 7z is installed
choco install 7zip.install

# Source of web-server layout
$source = 'https://www.free-css.com/assets/files/free-css-templates/download/page264/expertum.zip'

# Destination to save the file
$destination = "C:\Users\$env:username.$env:USERDOMAIN\Downloads\expertum.zip"
$unload = "C:\Users\$env:username.$env:USERDOMAIN\Downloads\extracted"

# mkdir \\magnaaro-srv\files\webserver

# Download the file
Invoke-WebRequest -Uri $source -OutFile $destination
# Extract files in directory where zip is located, name extracted folder extracted.
7z x $destination -y -oextracted

# dir \\magnaarokontroll.sec\files\webserver 
Copy-Item -Path $unload -Destination \\magnaarokontroll.sec\files\webserver\ -recurse -Force

Install-WindowsFeature -name Web-Server -IncludeManagementTools
# Copy files to correct folder 
Copy-Item -Path \\magnaarokontroll.sec\files\webserver\extracted\* -Destination "C:\inetpub\wwwroot" -recurse -Force

exit-PSsession