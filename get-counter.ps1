$scriptblock = { 

    $Counters = @(
        <#How much cpu is in use#>
        "\Processor(_Total)\% Processor Time", 
        <#Total RAM are the processes using#>
        "\Process(_Total)\Working Set",
        <#Number of requests disk isn't fulfilling due to activity spike or other issues#>
        "\LogicalDisk(_Total)\Current Disk Queue Length"
    ) 
    $Counters | Get-Counter -SampleInterval 60 -Continuous <#Interval can be changed as needed. Script can otherwise run in background#>
}

$csvPath = 'Documents\SRV-log.csv' <#Path can be changed as needed#>

Invoke-Command -ComputerName mmagnaaro-srv -ScriptBlock $scriptblock | ForEach-Object {
    $_.CounterSamples | ForEach-Object {
        [pscustomobject]@{
            TimeStamp = $_.TimeStamp
            Path      = $_.Path
            Value     = $_.CookedValue
        }
    }
} | Export-Csv -Path $home\$csvPath -Append -NoTypeInformation <#Exports/adds the information to csv-file#>
