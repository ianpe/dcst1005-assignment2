#1) This GPO will set the default home page(when clicking home button) in Microsoft Edge version 77, Windows 7 or later: 
#https://gpsearch.azurewebsites.net/#14671
New-GPO -Name "HomepageLocation" -Comment "Sets default homepage in Edge"
Set-GPRegistryValue -Name "HomepageLocation" -Key "HKLM\Software\Policies\Microsoft\Edge" -ValueName HomepageLocation -Type String -Value 129.241.98.87
Get-GPO -Name "HomepageLocation" | New-GPLink -Target "OU=onprem-desktops,OU=onprem-computers,DC=magnaarokontroll,DC=sec"

#2)This GPO will show an "contact IT"-mail in Windows Security, only available on Windows 10 and Windows Server: 
#https://gpsearch.azurewebsites.net/#13734 
New-GPO -Name "Presentation_EnterpriseCustomization_Email" -Comment "Shows who to contact in IT when problems occur."
Set-GPRegistryValue -Name "Presentation_EnterpriseCustomization_Email" -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender Security Center\Enterprise Customization" -ValueName Email -Type String -Value it@support.com
Get-GPO -Name "Presentation_EnterpriseCustomization_Email" | New-GPLink -Target "OU=onprem-desktops,OU=onprem-computers,DC=magnaarokontroll,DC=sec"
Set-GPLink -Name "Presentation_EnterpriseCustomization_Email" -Target "DC=magnaarokontroll,DC=sec" -Enforced Yes

#3)This GPO will allow you to remotely access computers with RDP services. You have to assign users to the Remote Desktop Users on target computer for this GPO to function. 
#https://gpsearch.azurewebsites.net/#2481
New-GPO -Name "fDenyTSConnections" -Comment "Allows users to connect remotely by using RDP services"
Set-GPRegistryValue -Name "fDenyTSConnections" -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" -ValueName fDenyTSConnections -Type DWord -Value 0
Get-GPO -Name "fDenyTSConnections" | New-GPLink -Target "OU=onprem-desktops,OU=onprem-computers,DC=magnaarokontroll,DC=sec" 
Get-GPO -Name "fDenyTSConnections" | New-GPLink -Target "OU=onprem-laptops,OU=onprem-computers,DC=magnaarokontroll,DC=sec" 

#4)This GPO will still let OneDrive sync while battery saving mode is on: https://gpsearch.azurewebsites.net/#14424
New-GPO -Name "DisablePauseOnBatterySaver" -Comment "When employees turn on battery saving mode the syncing to OneDrive will still continue."
Set-GPRegistryValue -Name "DisablePauseOnBatterySaver" -Key "HKCU\SOFTWARE\Policies\Microsoft\OneDrive" -ValueName DisablePauseOnBatterySaver -Type Dword -Value 1
Get-GPO -Name "DisablePauseOnBatterySaver" | New-GPLink -Target "OU=Accounting,OU=onprem-users,DC=magnaarokontroll,DC=sec" 
Get-GPO -Name "DisablePauseOnBatterySaver" | New-GPLink -Target "OU=Cleaning,OU=onprem-users,DC=magnaarokontroll,DC=sec" 
Get-GPO -Name "DisablePauseOnBatterySaver" | New-GPLink -Target "OU=Development,OU=onprem-users,DC=magnaarokontroll,DC=sec" 
Get-GPO -Name "DisablePauseOnBatterySaver" | New-GPLink -Target "OU=HR,OU=onprem-users,DC=magnaarokontroll,DC=sec" 
Get-GPO -Name "DisablePauseOnBatterySaver" | New-GPLink -Target "OU=IT,OU=onprem-users,DC=magnaarokontroll,DC=sec" 

#5)This GPO will turn on recommended updates via Automatic updates: https://gpsearch.azurewebsites.net/#2796
New-GPO -Name "IncludeRecommendedUpdates" -Comment "Installs both recomemended and important updates from Windows Update"
Set-GPRegistryValue -Name "IncludeRecommendedUpdates" -Key "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU" -ValueName IncludeRecommendedUpdates -Type Dword -Value 1
Get-GPO -Name "IncludeRecommendedUpdates" | New-GPLink -Target "OU=onprem-desktops,OU=onprem-computers,DC=magnaarokontroll,DC=sec"


#Force group policy update on all intended computers. Note that all targets computers must be turned on.
Get-ADComputer -Filter * -SearchBase "OU=onprem-users,DC=magnaarokontroll,DC=sec" | Foreach-Object {Invoke-GPUpdate -Computer $_.name -Force -RandomDelayInMinutes 0}
Get-ADComputer -Filter * -SearchBase "OU=onprem-computers,DC=magnaarokontroll,DC=sec" | Foreach-Object {Invoke-GPUpdate -Computer $_.name -Force -RandomDelayInMinutes 0}
