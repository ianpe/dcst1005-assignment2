
### DEVICE INFORMATION ###

# DOMAIN: magnaarokontroll.sec
# DC: magnaaro-dc1
# SERVER: magnaaro-srv
# CLIENT: magnaaro-cli1




### CREATING AN OU STURCTURE ###

# Created global timestamp to be used later
$scriptStartTimestamp = Get-Date

# Declaring a new PS sessiion to DC-computer
$dcSession = New-PSSession -ComputerName magnaaro-dc1

# Entering PS Session on the domain controller
Enter-PSSession $dcSession

# Checking for Active Directory module and importing it
Get-Module -ListAvailable -Name "*ActiveDirectory" | Import-Module

# Creating new OUs for different departments. These will contain the company employees
New-ADOrganizationalUnit "onprem-users" -Description "Containing OUs and users"
New-ADOrganizationalUnit "IT" -Description "OU - IT Department" -Path "OU=onprem-users,DC=magnaarokontroll,DC=sec"
New-ADOrganizationalUnit "HR" -Description "OU - HR Department" -Path "OU=onprem-users,DC=magnaarokontroll,DC=sec"
New-ADOrganizationalUnit "Development" -Description "OU - Development Team" -Path "OU=onprem-users,DC=magnaarokontroll,DC=sec"
New-ADOrganizationalUnit "Accounting" -Description "OU - Accounting Department" -Path "OU=onprem-users,DC=magnaarokontroll,DC=sec"
New-ADOrganizationalUnit "Cleaning" -Description "OU - Cleaing Staff" -Path "OU=onprem-users,DC=magnaarokontroll,DC=sec"

# Creating OUs for the company's computers. Both desktops and personal laptips
New-ADOrganizationalUnit "onprem-computers" -Description "Containing all computers"
New-ADOrganizationalUnit "onprem-servers" -Description "OU - Servers" -Path "OU=onprem-computers,DC=magnaarokontroll,DC=sec"
New-ADOrganizationalUnit "onprem-desktops" -Description "OU - Desktops" -Path "OU=onprem-computers,DC=magnaarokontroll,DC=sec"
New-ADOrganizationalUnit "onprem-laptops" -Description "OU - Laptops" -Path "OU=onprem-computers,DC=magnaarokontroll,DC=sec"

# Creating OUs for different resources within the company
New-ADOrganizationalUnit "onprem-resources" -Description "Containing resources and tools on prem" -Path "DC=magnaarokontroll,DC=sec"
New-ADOrganizationalUnit "onprem-printers" -Description "OU - Printers" -Path "OU=onprem-resources,DC=magnaarokontroll,DC=sec"
New-ADOrganizationalUnit "onprem-accesscards" -Description "OU - Doors" -Path "OU=onprem-resources,DC=magnaarokontroll,DC=sec"

# Moving the devices in the domain to their designated OUs
Get-ADComputer "magnaaro-srv" | Move-ADObject -TargetPath "OU=onprem-servers,OU=onprem-computers,DC=magnaarokontroll,DC=sec" -verbose
Get-ADComputer "magnaaro-cli1" | Move-ADObject -TargetPath "OU=onprem-laptops,OU=onprem-computers,DC=magnaarokontroll,DC=sec" -verbose




### CREATING GROUPS FOR USERS ###

# Creating global groups for all departments in the company
New-ADGroup -GroupCategory Security -GroupScope Global -Name "IT_Group" -Path "OU=IT,OU=onprem-users,DC=magnaarokontroll,DC=sec" -SamAccountName "IT_Group"
New-ADGroup -GroupCategory Security -GroupScope Global -Name "HR_Group" -Path "OU=HR,OU=onprem-users,DC=magnaarokontroll,DC=sec" -SamAccountName "HR_Group"
New-ADGroup -GroupCategory Security -GroupScope Global -Name "Development_Group" -Path "OU=Development,OU=onprem-users,DC=magnaarokontroll,DC=sec" -SamAccountName "Development_Group"
New-ADGroup -GroupCategory Security -GroupScope Global -Name "Accounting_Group" -Path "OU=Accounting,OU=onprem-users,DC=magnaarokontroll,DC=sec" -SamAccountName "Accounting_Group"
New-ADGroup -GroupCategory Security -GroupScope Global -Name "Cleaning_Group" -Path "OU=Cleaning,OU=onprem-users,DC=magnaarokontroll,DC=sec" -SamAccountName "Cleaning_Group"

# Getting all the groups in the domain
Get-ADGroup -filter "Name -like '*_Group'" | Select-Object name, DistinguishedName




### CREATING GROUPS FOR RESOURCES ###

# Declaring the resources that needs a group
$Resources = @("Printers", "AccessCards")

# Creating groups for the company's resources with the path respective to the resource
$Resources | ForEach-Object { $groupName = $_ + "_Group"; $OUName = ("onprem-" + $_).ToLower(); New-ADGroup -name $groupName -Path "OU=$OUName,OU=onprem-resources,DC=magnaarokontroll,DC=sec" -GroupCategory Security -GroupScope DomainLocal }




### CREATING USERS ###

# Declaring the number of user creation attempts
$userAddCount = 0

# Importing the CSV file which contains new users
$ADUsers = Import-csv "$PSScriptRoot\csv\users.csv" -Delimiter ";" # Change 'users-test.csv' to 'users.csv'

#For each object $User do this...
foreach ($User in $ADUsers) {
    # Setting full name
    $fullName = $User.GivenName + " " + $User.SurName;

    # Creating username based on first three letters of first and last name
    $username = $User.GivenName.Substring(0, 3) + $User.SurName.Substring(0, 3)
    
    # If the username allready exists. Do this...
    if (@(Get-ADUser -Filter {SamAccountName -eq $username}).Count -ge 1) {
        # Import letters from name once again, but with an offset by one
        $username = $User.GivenName.Substring(1, 3) + $User.SurName.Substring(1, 3)
    }
    
    # Set username to lower case and replace all norwegian characters
    $username = $username.ToLower() -replace "æ", "ae" -replace "å", "aa" -replace "ø", "oe"
    
    # Setting principal name
    $principalName = $username + "@magnaarokontroll.sec"
    
    # Create user with given variables
    New-ADUser `
        -SamAccountName $username `
        -UserPrincipalName $principalName `
        -Name "$fullname" `
        -GivenName $User.GivenName `
        -Surname $User.SurName `
        -Enabled $true `
        -ChangePasswordAtLogon $false `
        -DisplayName "$fullname" `
        -Department $User.Department `
        -Path $User.Path `
        -AccountPassword (ConvertTo-SecureString $User.Password -AsPlainText -Force)

    # Incrementing the number for each user
    $userAddCount++
}




### ASSIGNING USERS TO GROUP ###

# Declaring array containing all departments
$departments = @("IT", "HR", "Development", "Accounting", "Cleaning")

# Running loop for all departments
$departments | ForEach-Object {
    # Storing the current department in variable
    $thisDepartment = $_
    
    # Storing the respective group for the department
    $thisGroup = $_ + "_Group"
    
    # Listing all the users withing the department
    $userList = Get-ADUser -Filter * -Properties Department | Where-Object {$_.Department -Like "$thisDepartment"} | Select-Object SamAccountName
    
    # Adding the users to the group
    Add-ADGroupMember -Identity $thisGroup -Members $userList
    
    # Displaying all the added users
    Get-ADGroupMember -Identity $thisGroup | Select-Object Name
}




### CREATING SHARED FOLDERS ###

# Exiting PS Session
Exit-PSSession

# Listing all the folders to be created
$folders = @("C:\shares\ISO-Files","C:\shares\HR", "C:\shares\Development Team", "C:\shares\Accounting")

# Creating the folders locally on server
mkdir -Path $folders

# Creating new SMBShare folders
$folders | ForEach-Object {$sharename = (Get-Item $_).Name; New-SmbShare -Name $sharename -Path $_ -FullAccess Everyone}

# Creating namespaces for folders
$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).Name; $DFsPath = ("\\magnaarokontroll.sec\files\" + $name); $targetPath = ("\\magnaaro-srv\" + $name); New-DfsnFolderTarget -Path $DFsPath -TargetPath $targetPath}

# Listing all the shared folders that should have limited access
$limitedFolders = $folders | Where-Object {$_ -notlike "*ISO-Files*"}

# Adding a access rule for groups to their respective shared folder
$limitedFolders | ForEach-Object {
    
    # Declaring the name of the folder
    $folderName = $_.split("\")[2]

    # Getting the ACL for the folder
    $acl = Get-Acl "\\magnaarokontroll\files\$folderName"

    # Determining the name of the group depending on the file
    switch ($folderName) {
        "HR" {$group = "HR_Group"}
        "Development Team" {$group = "Development_Group"}
        "Accounting" {$group = "Accounting_Group"}
    }

    # Creating new rule for the group, giving them full control
    $rule = New-Object System.Security.AccessControl.FileSystemAccessRule("magnaarokontroll\$group", "FullControl", "Allow")
    $acl.SetAccessRule($rule)

    # Assigning the rule to the correct folder
    $acl | Set-Acl -Path "\\magnaarokontroll\files\$folderName"
}

# Removing all inherited rights for the intended restricted folders
$limitedFolders | ForEach-Object {

    # Declaring the name of the folder
    $folderName = $_.split("\")[2]

    # Getting the ACL
    $acl = Get-Acl -Path "\\magnaarokontroll\files\$folderName"

    # Setting 'isProtected' and 'preserveInheritance' to true
    $acl.SetAccessRuleProtection($true, $true)

    # Assigning new rule to folder
    $acl | Set-Acl -Path "\\magnaarokontroll\files\$folderName"
}

# Removing ACL for standard users from resticted folders
$limitedFolders | ForEach-Object {

    # Declaring the name of the folder
    $folderName = $_.split("\")[2]

    # Getting the ACL
    $acl = Get-Acl "\\magnaarokontroll\files\$folderName"

    # Retrieving the ACL rules for 'BUILTIN\Users' and remove it
    $acl.Access | Where-Object {$_.IdentityReference -eq "BUILTIN\Users"} | ForEach-Object {$acl.RemoveAccessRuleSpecific($_)}

    # Assigning new rule to folder
    Set-Acl "\\magnaarokontroll\files\$folderName" $acl

    # Writing to console the new ACL access rules for all folders that has been modified
    (Get-ACL -Path "\\magnaarokontroll\files\$folderName").Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize
}




# Exiting PS Session
Exit-PSSession

# Retrieving the amount of events with the 4720 id from the domain controller
$userConfirmedCount = Invoke-Command -ComputerName "magnaaro-dc1" -ScriptBlock {
    # Command that counts the amount of "Added User Account" events added since the script started
    (Get-WinEvent -FilterHashtable @{ LogName="Security"; StartTime=$Using:scriptStartTimeStamp; id=4720} -ErrorAction SilentlyContinue).Count | Write-Output
}

# Write to host the number of attempts and the number of new users
Write-Host "Attempts of Adding User:    #$userAddCount"
Write-Host "Confirmed Users Added:      #$userConfirmedCount"